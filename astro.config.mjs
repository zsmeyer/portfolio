import { defineConfig } from 'astro/config';
import icon from "astro-icon";

// https://astro.build/config
export default defineConfig({
  site: 'https://zsmeyer.gitlab.io',
  base: '/portfolio',
  outDir: 'public',
  publicDir: 'static',
  integrations: [icon()],
  markdown: {
      shikiConfig: {
      // Choose from Shiki's built-in themes (or add your own)
      // https://shiki.style/themes
      theme: 'ayu-dark'
      }
  },
  build: {inlineStylesheets: 'always'}
  });
