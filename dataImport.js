let csvToJson = require('convert-csv-to-json');

let fileInputName = './entries.csv';
let fileOutputName = './src/content/experience/entries.json';

csvToJson.supportQuotedField(true)
	.formatValueByType()
	.fieldDelimiter(',')
	.parseSubArray('*','; ')
	.generateJsonFileFromCsv(fileInputName,fileOutputName);
