# Zachary Meyer's portfolio

This repo hosts the source for my personal [portfolio site](https://zsmeyer.gitlab.io/portfolio).

I wanted to try out [Bun](https://bun.sh) as a potentially faster alternative to npm, so you'll need
that if you want to build/run the project locally. 

```sh
git clone https://gitlab.com/zsmeyer/portfolio.git
cd portfolio

# This is similar to npm ci
bun install --frozen-lockfile

bun run dev # For a local dev server
# or
bun run build # To build locally
```


