---
title: "Projects"
---

# Selected Projects

## W2 Calculator

[w2calculator.fso.arizona.edu](https://w2calculator.fso.arizona.edu)

This application was created at the request of the payroll department. Each
year during tax season, their staff had to field a similar set of questions
from employees about why the total boxes on their W2 were different from each
other and from what they knew their salary to be. 

Nearly everything in the public-facing view of the application can be modified
by payroll staff who are not primarily in a technical role. This allows them to
make changes to the application in response to employee feedback and changing
tax law without having to wait for application development resources.

This was the first application we converted from Vue 2 to Vue 3. 

We're currently working on improving the performance of the application.

### Example Code

This was my first "real" experience with Javascript, and I actually wrote the
entire "public-facing" part of the app in vanilla html/Javascript before moving
it into Vue.  

I enjoyed writing and re-writing the Javascript responsible for handling user
input. I felt like the requirement for an admin interface made this project
just that much more challenging than a typical "calculator" tutorial you might
see when learning a new programming language. For example, our colleagues in
the payroll department wanted to make it possible for admins to change the
inputs that appear on screen for users to fill in (as the IRS can often change
what categories are taxable or not). They also wanted admins to be able to
impose limits on certain totals (again, because the IRS might change the amount
of income subject to a certain tax from year to year).

The following Javascript functions run each time a user changes an input in the
calculator. They update the "total" boxes at the bottom of the application so
that users can quickly see how changes to the numbers they input affect the W2
totals.

```js
    runCalculations() {
    // totalBoxes is an array containing information about
    // each total/result section of the app. It is loaded 
    // from the backend when a user first navigates to the app.
    // Admins can modify various details about the total boxes, 
    // including how many appear in the app, which inputs are included,
    // limits to amounts, and help text. 
      for (const total of this.totalBoxes) {
        total.credits = this.inputs
          // Inputs can be categorized as either a debit or
          // credit by Admins, which controls whether a particular
          // input is added to or subtracted from a total.
          .filter((input) => input.debit == "credit")
          .filter((input) => total.inputs.includes(input.id))

        total.creditsValues = []
        total.credits.map((element) =>
          total.creditsValues.push(Number(element.value)),
        )
        total.creditsSum = this.plusReduce(total.creditsValues)

        total.debits = this.inputs
          .filter((input) => input.debit == "debit")
          .filter((input) => total.inputs.includes(input.id))
        total.debitsSum = this.plusReduce(total.debits)

        total.debitsValues = []
        total.debits.map((element) =>
          total.debitsValues.push(Number(element.value)),
        )
        total.debitsSum = this.plusReduce(total.debitsValues)

        total.rawTotal = total.creditsSum - total.debitsSum

        if (total.limit == 0) {
          total.grandTotal = total.rawTotal
        } else if (total.limit > 0) {
          total.grandTotal = Math.min(total.limit, total.rawTotal)
        }
      }
    }
```


## Tuition Calculator

[tuitioncalculator.fso.arizona.edu](https://tuitioncalculator.fso.arizona.edu)

This application had already existed for a few years when I started at the
University of Arizona. The public-facing side of the application is backed by a
fairly complicated "admin" side that allows Bursar staff to store and modify
all the combinations of tuition, fee, program, and student status information
for each semester.

### Example Code

TODO

## Virtual Cards

This application was created at the request of the accounts payable department.
As part of their process of paying university vendors and/or partners, they
needed to submit orders for "virtual cards" to a banking partner. These orders
were required to be specially formatted csv files that, once uploaded to the
banking portal, would generate pre-paid debit cards that would be emailed to
each vendor specified in the file. 

The accounts payable department reached out to our team because they were
transitioning this process from one banking partner to another and needed to
ensure they were conforming to the new partner's requirements. In addition,
they wanted to explore possible improvements to their existing process, which
at the time required several powershell scripts and manual spreadsheet edits by
their staff.

We created an application that enabled the accounts payable team to move the
existing process, and other parts of their workflow related to virtual cards,
into a uniform interface. For example, the application enabled staff to upload
clearing reports from the banking partner and automatically reconciled this
data with the virtual card orders. Previously, staff had to do this manually,
with a complex series of email mailboxes and spreadsheets.

### Example Code

TODO

## Google Workspace Storage Analysis

TODO

## Nix/NixOS Configuration

TODO

## Question Preview in English for Academic Purposes Listening Assessment

[Materials (raw data, analysis code, manuscript) on the Open Science Framework](https://osf.io/c8ksg/)

[Article page in the International Journal of Listening](https://doi.org/10.1080/10904018.2022.2029705)

This paper is what I think of as the turning point of my career. It was the
culmination of years of work as an applied linguist and English teacher, but
also the first time I put any code out into the world. Although I would
probably approach parts of the technical implementation differently today (I
think most especially related to including more information about software
versions and environments to facilitate replication), I am still proud of what
we produced and the fact that we shared all of our materials on an open
platform.
