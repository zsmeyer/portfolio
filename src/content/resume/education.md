---
title: "Education"
---

## Education

### M.A., Second Language Studies (Applied Linguistics)
Indiana University, 2015

### B.A., Spanish
Indiana University, 2013
