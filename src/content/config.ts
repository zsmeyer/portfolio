// 1. Import utilities from `astro:content`
import { defineCollection, z } from 'astro:content';
// 2. Define your collection(s)
const resumeCollection = defineCollection({
	type: 'content', // v2.5.0 and later
  schema: z.object({
    title: z.string().optional(),
  }), /* ... */ });

//const resumeData = defineCollection({
//	schema: z.object({
//		section: z.string(),
//			title: z.string(),
//			institution: z.string().optional(),
//			start: z.number().optional(),
//			end: z.number().optional(),
//			description_1: z.string().optional(),
//			description_2: z.string().optional(),
//			description_3: z.string().optional(),
//			description_4: z.string().optional(),
//			description_5: z.string().optional,
//			description_6: z.string().optional,
//			in_resume: z.boolean()
//	})
//});
		
// 3. Export a single `collections` object to register your collection(s)
//    This key should match your collection directory name in "src/content"
export const collections = {
  'resume': resumeCollection,
};
